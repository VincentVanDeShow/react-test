import {useState, useEffect } from 'react';

// local cache
const localCache = {};

export default function useBreedList(animal) {
    // set breedlist
    const [breedList, setBreedList] = useState([]);
    //  set status
    const [status, setStatus] = useState('unloaded');

    // use effect
    useEffect(() => {
        if (!animal) {
            // if there isnt an animal just get the breedlist
            setBreedList([])
        } else if (localCache[animal]) {
            // check if the animal is in the local cache
            setBreedList(localCache[animal])
        } else {
            // if not get a new breedList
            requestBreedList();
        }

        // make a call to get the new breedlist
        async function requestBreedList() {
            // asuming there isn't anything the breedlist is empty + you don't want a different animal like cat... with the breed husky
            setBreedList([]);
            // set status loading
            setStatus('loading');

            // do a call to the api
            const res = await fetch(
                `http://pets-v2.dev-apis.com/breeds?animal=${animal}`
            )
            // set it to json
            const json = await res.json();
                // update the localCache animal
            localCache[animal] = json.breeds || [];
            // add it to the BreedList
            setBreedList(localCache[animal]);
            // update our status
            setStatus('loaded');
        }
        // we want to do this everytime there is a call for an animal.
    }, [animal])
    // we need to return the breedlist and the status of the CustomHook
    return [breedList, status];
}