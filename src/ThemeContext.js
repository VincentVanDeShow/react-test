import { createContext } from "react";

// Default value + function -> the empty function will be used for the useState
const ThemeContext = createContext(["green", () => {}]);

export default ThemeContext