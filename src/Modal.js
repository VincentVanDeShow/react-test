import { useEffect, useRef } from "react";
import { createPortal } from "react-dom";

const modalRoot = document.getElementById('modal');

const Modal = ({ children }) => {
    const elRef = useRef(null);
    if (!elRef.current) {
        // if there isnt a div around create this div
        elRef.current = document.createElement('div');
    }

    useEffect(() => {
        // we are appending this to the dom
        modalRoot.appendChild(elRef.current);
        // when you are done remove this from the dom
        return () => modalRoot.removeChild(elRef.current);
    }, [])

    return createPortal(<div>{children}</div>, elRef.current);
}

export default Modal;