import { Component } from "react";
import { withRouter } from "react-router-dom";
import Carousel from "./Carousel";
import ErrorBoundary from "./ErrorBoundary";
import ThemeContext from "./ThemeContext";
import Modal from "./Modal";

// lifecycle methods.
// ** me making an class just to learn classes **
class Details extends Component {
    // when calling an constructor you always have to use super();
    constructor() {
        super();
        // set loading state
        this.state = { loading: true, showModal: false }
    }
    async componentDidMount () {
        // get the data for the page.
        const res = await fetch (
            `http://pets-v2.dev-apis.com/pets?id=${this.props.match.params.id}`
        );
        // set to json
        const json = await res.json();
        // update setState with ObjectAssign so we don't have to set every individual key.
        this.setState(
            Object.assign(
                {
                    loading: false,
                },
                // it's set up as an []
                json.pets[0]
            )
        );
    }
    // toggleModal switches the modal status
    toggleModal = () => this.setState({showModal: !this.state.showModal});
    // when you press yes you go to a different page.
    adopt = () => (window.location = 'http://bit.ly/pet-adopt');
    render () {
        // set the variable for the state, so it's easier to set this in the html
        const { animal, breed, city, state, description, name, images, showModal } = this.state;
        return (
            <div className="details">
                <Carousel images={images} />
                <div>
                    <h1>{name}</h1>
                    <h2>{`${animal} - ${breed} - ${city}, ${state}`}</h2>
                    <ThemeContext.Consumer>
                        {([theme]) => (
                            <button
                                style={{ backgroundColor: theme }}
                                onClick={this.toggleModal}
                            >Adopt {name}</button>
                        )}
                    </ThemeContext.Consumer>
                    <p>{description}</p>
                    {/* adding a modal */}
                    {
                        showModal ? (
                            <Modal>
                                <div>
                                    <h1>Would you like to adopt {name}?</h1>
                                    <div className="buttons">
                                        <button onClick={this.adopt}>Yes</button>
                                        <button onClick={this.toggleModal}>No</button>
                                    </div>
                                </div>
                            </Modal>
                        )
                    : null}
                </div>
            </div>
        )
    }
}
const DetailsWithRouter = withRouter(Details);

export default function DetailsWithErrorBoundary() {
    return (
        <ErrorBoundary>
            <DetailsWithRouter />
        </ErrorBoundary>
    )
} ;